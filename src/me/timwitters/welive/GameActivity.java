package me.timwitters.welive;

import java.io.IOException;

import com.google.gson.Gson;

import me.timwitters.game.Game;
import me.timwitters.game.Player;
import me.timwitters.interfaces.ATInterface;
import me.timwitters.interfaces.BoardViewInterface;
import me.timwitters.interfaces.JInterface;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import edu.vub.at.android.util.IATAndroid;
import edu.vub.at.exceptions.InterpreterException;

public class GameActivity extends Activity implements JInterface, BoardViewInterface {
	private IATAndroid iat;
	private ATInterface ati;
	private Handler mHandler;
	private Player myPlayer;
	private Game game = new Game(10, 10);
	private BoardView boardView;
	private TextView txtLifeBank;
	private TextView txtScore;
	private TextView txtPlayerName;
	private ImageView imgLeader;
	private ProgressBar generationBar;
	private Button btnLiveBonus;
	private Button btnShowResults;
	private boolean starter = true;
	private boolean leader = false;
	private boolean running = false;
	private Thread gameThread;
	private SharedPreferences preferences;
	   
	//All possible messages for the handler
	private static final int MSG_GetPlayerId = 0;
	private static final int MSG_SetPlayerId = 1;
	private static final int MSG_TakeCell = 2;
	private static final int MSG_UpdateBoard = 3;
	private static final int MSG_SwitchStarter = 4;
	private static final int MSG_UpdatePlayerCount = 5;
	private static final int MSG_UpdateBoardRemote = 6;
	private static final int MSG_StartElection = 7;
	private static final int MSG_GoOffline = 8;
	private static final int MSG_GoOnline = 9;
	private static final int MSG_StartGame = 10;
	private static final int MSG_StartedGame = 11;
	private static final int MSG_LogATState = 12;
	private static final int MSG_ToggleLeader = 13;
	private static final int MSG_SendRestartAll = 14;
	private static final int MSG_StopGame = 15;
	private static final int MSG_SendPlayer = 16;
	   
	private class CellMessage {
		public int playerId;
		public int indexId;
		public long timestamp;
		public CellMessage(int playerId, int indexId, long timestamp) {
			this.playerId = playerId;
			this.indexId = indexId;
			this.timestamp = timestamp;
		}
	}
	  
	// UI Actions
	@Override
	public void clickedRectangle(int index) {
		System.out.println("Clicked: " + index);
		if(game.getLiveBank() > 0) {	
			long timeMili = System.currentTimeMillis();
			game.takeCell(myPlayer.getId(), index, timeMili);
			//game.setLiveBank(game.getLiveBank() - 1);	
			mHandler.sendMessage(Message.obtain(mHandler, MSG_UpdateBoard));
			mHandler.sendMessage(Message.obtain(mHandler, MSG_TakeCell, new CellMessage(myPlayer.getId(), index, timeMili)));
		} else {
			System.out.println("Life bank empty");
		}	
	}  
	 
	public void logGame(View view) { 
		System.out.println("LiveBank: " + game.getLiveBank());
		System.out.println("Generation Cound: " + game.getGenerationCount());
		System.out.println("Player Count: " + game.getPlayers().size());
		System.out.println("isLeader: " + leader);
		System.out.println("running: " + running);
	}  
	
	public void logAT(View view) {
		mHandler.sendMessage(Message.obtain(mHandler, MSG_LogATState));
	}
	
	public void startElection(View view) {
		mHandler.sendMessage(Message.obtain(mHandler, MSG_StartElection));
	}
	
	public void goOnline(View view) {
		mHandler.sendMessage(Message.obtain(mHandler, MSG_GoOnline));
	}
	
	public void clickLiveBonus(View view) {
		game.setLiveBank(game.getLiveBank() + 5);
		btnLiveBonus.setVisibility(View.GONE);
		mHandler.sendMessage(Message.obtain(mHandler, MSG_UpdateBoard));
	}
	
	public void clickRestartGame(View view) {
		restartGame();
		mHandler.sendMessage(Message.obtain(mHandler, MSG_SendRestartAll));
	}
	
	public void goOffline(View view) {
		mHandler.sendMessage(Message.obtain(mHandler, MSG_GoOffline));
	}
	
	public void clickNextGeneration(View view) {
		sendNextGeneration();
	}
	
	public void clickStartGame(View view) {
		mHandler.sendMessage(Message.obtain(mHandler, MSG_StartGame));
	}
	
	public void clickShowResutls(View view) {
		Gson gson = new Gson();
		Intent i = new Intent(this, ResultsActivity.class);
		i.putExtra("Players", gson.toJson(game.getPlayers()));
		startActivity(i);
	}
	//END UI Actions
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game);
		boardView = (BoardView) findViewById(R.id.boardView);
		boardView.setSize(game.getGameWidth(), game.getGameHeight());
		txtLifeBank = (TextView) findViewById(R.id.txtLifeBankCount);
		txtScore = (TextView) findViewById(R.id.txtPlayerScore);
		txtPlayerName = (TextView) findViewById(R.id.txtPlayerNameValue);
		imgLeader = (ImageView) findViewById(R.id.imgLeader);
		generationBar = (ProgressBar) findViewById(R.id.GenerationProcess);
		btnLiveBonus = (Button) findViewById(R.id.LiveBonus);
		btnShowResults = (Button) findViewById(R.id.btnShowResults);
		
		//Setting Username from persistent key value store
		preferences = getSharedPreferences("KeyStore",MODE_PRIVATE);
		String username = preferences.getString("Username", "");
		System.out.println("Username:" + username);
		txtPlayerName.setText(username);
		
		Intent i = new Intent(this, weLiveAssetInstaller.class);
		startActivityForResult(i, 1);
		
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		System.out.println("onActivityResult");
		
		new StartIATTask().execute((Void)null);
		
		LooperThread lt = new LooperThread();
		lt.start();
		mHandler = lt.mHandler;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.game, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	 // Starts up the AmbientTalk interpreter and interprets the code provided in assets/atlib/weLive/weLive.at
    public class StartIATTask extends AsyncTask<Void,String,Void> {
    	@Override
    	protected Void doInBackground(Void... params) {
    		try { 
    			iat = IATAndroid.create(GameActivity.this);
    			iat.evalAndPrint("import /.weLive.player.makePlayer()", System.err);
    			mHandler.sendMessage(Message.obtain(mHandler, MSG_SetPlayerId));
    		} catch (IOException e) {
    			e.printStackTrace();
    		} catch (InterpreterException e) {
    			Log.e("AmbientTalk","Could not start IAT",e);
    		}
    		return null;
    	}
    }  
    
    private void startGameLoopThread() {
    	final long generationTimer = 20; //In seconds
    	gameThread = new Thread() {
    		public void run() {
    			while(running) {
    				try {
    					long beginLoop = System.currentTimeMillis();
    					if(leader) {
    						sendNextGeneration();
    					}
    					long endLoop = System.currentTimeMillis();
    					long difference = endLoop - beginLoop;
    					sleep((generationTimer*1000)-difference);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
    			}
    		}
    	};
    	gameThread.start();
    }
    // Call the AmbientTalk methods in a separate thread to avoid blocking the UI.
 	private class LooperThread extends Thread {
 		public Handler mHandler = new Handler() {
 			public void handleMessage(Message msg) {
 				if (ati != null) {
 					System.out.println("Handeling Message: " + msg.what);
 					switch (msg.what) {
 	 				//Handling all different calls to the ATInterface
 	 				case MSG_GetPlayerId:
 	 					System.out.println("Player id from AT: " + ati.getPlayerId()); break;
 	 				case MSG_SetPlayerId:
 	 					TextView playerId = (TextView) findViewById(R.id.PlayerId);
 	 					Integer i = ati.getPlayerId();
 	 					System.out.println("Player Id returned: " + i);
 	 					playerId.setText("Player Id: " + i.toString());
 	 					break; 
 	 				case MSG_TakeCell:
 	 					CellMessage cellMessage = (CellMessage) msg.obj;
 	 					ati.sendTakeCell(cellMessage.playerId, cellMessage.indexId, cellMessage.timestamp);
 	 					break; 
 	 				case MSG_UpdateBoard:
 	 					System.out.println("Updating Board: " + game.getGenerationPercent());
 	 					txtLifeBank.setText(Integer.toString(game.getLiveBank()));
 	 					txtScore.setText(Integer.toString(game.getScore()));
 	 					generationBar.setProgress(game.getGenerationPercent());
 	 					boardView.redraw(game); break; 
 	 				case MSG_UpdateBoardRemote:
 	 					ati.updateBoardRemote((String) msg.obj, game.getGenerationCount()); break;
 	 				case MSG_SwitchStarter:
 	 					Button btn = (Button) findViewById(R.id.StartGame);
 	 					Button btnRest = (Button) findViewById(R.id.ResetGame);
 	 					if ((boolean) msg.obj) {
 	 						btnRest.setVisibility(View.VISIBLE);
 	 						btn.setVisibility(View.GONE);
 	 					} else {
 	 						btnRest.setVisibility(View.GONE);
 	 						btn.setVisibility(View.VISIBLE);
 	 					}
 	 					break;  
 	 				case MSG_UpdatePlayerCount:
 	 					TextView count = (TextView) findViewById(R.id.PlayerCount);
 	 					count.setText(Integer.toString(game.getPlayers().size()));
 	 					break;
 	 				case MSG_StartElection:
 	 					System.out.println("Election Started");
 	 					ati.startElection(); break;
 	 				case MSG_GoOffline:
 	 					ati.goOffline(); break;
 	 				case MSG_GoOnline:
 	 					ati.goOnline(); break;
 	 				case MSG_StartGame:
 	 					txtLifeBank.setText(Integer.toString(game.getLiveBank()));
 	 					boardView.setTouchable(true);
 	 					setStarter(true);
 	 					btnLiveBonus.setVisibility(View.VISIBLE);
 	 					running = true;
 	 					startGameLoopThread();
 	 					ati.startGame(); break;
 	 				case MSG_StartedGame:
 	 					setStarter(true);
 	 					running = true;
 	 					btnLiveBonus.setVisibility(View.VISIBLE);
 	 					startGameLoopThread();
 	 					txtLifeBank.setText(Integer.toString(game.getLiveBank()));
 	 					boardView.setTouchable(true);
 	 					break;
 	 				case MSG_LogATState:
 	 					ati.logATState(); break;
 	 				case MSG_ToggleLeader:
 	 					imgLeader.setVisibility((leader)? View.VISIBLE: View.GONE); break;
 	 				case MSG_SendRestartAll:
 	 					ati.restartGame(); break;
 	 				case MSG_StopGame:
 	 					running = false;
 	 					leader = false;
 	 					btnShowResults.setVisibility(View.VISIBLE);
 	 					if ((boolean) msg.obj) {
 	 						ati.stopGame();
 	 					}
 	 					break;
 	 				case MSG_SendPlayer:
 	 					ati.sendPlayer(game.getMyPlayerJson()); break;
 					}
 					
 				} else {
 					System.out.println("ATI not yet initiated"); 
 				}
 			}
 		};

 		public void run() {
 			Looper.prepare();
 			Looper.loop();
 		}
 	}

	@Override
	public void printInJava(String string) {
		System.out.println("printInJava()" + string);	
	}

	@Override
	public JInterface registerATApp(ATInterface ati) {
		System.out.println("RegisterATApp");
		this.ati = ati;
		return this;
	}
  
	@Override
	public void newATPlayer(int playerId) {
		System.out.println("New Player Received from AT");
		Player player = new Player();
		player.setId(playerId);
		player.setScore(0);
		player.setName(txtPlayerName.getText().toString());
		game.newPlayer(player);
		myPlayer = player;
		mHandler.sendMessage(Message.obtain(mHandler, MSG_UpdatePlayerCount));
	}
 
	@Override
	public void newRemoteATPlayer(int playerId) {
		System.out.println("Addring Remote Player");
		Player player = new Player();
		player.setId(playerId);
		player.setName("Other Player");
		player.setScore(0);
		game.newPlayer(player);
		mHandler.sendMessage(Message.obtain(mHandler, MSG_UpdatePlayerCount));
	} 
 
	@Override
	public void updateBoard(String jsonCells, int generationCount) {
		System.out.println("Updated board received from AT");
		game.updateBoardJson(jsonCells);
		game.setGenerationCount(generationCount);
		mHandler.sendMessage(Message.obtain(mHandler, MSG_UpdateBoard));
	}

	@Override
	public void takeCell(int playerId, int index, long timestamp) {
		System.out.println("Remote: " + playerId + " takin cell: " + index);
		game.takeCell(playerId, index, timestamp);
		mHandler.sendMessage(Message.obtain(mHandler, MSG_UpdateBoard));
	}

	public boolean isStarter() {
		return starter;
	}

	public void setStarter(boolean starter) {
		this.starter = starter;
		mHandler.sendMessage(Message.obtain(mHandler, MSG_SwitchStarter, starter));
	}

	public boolean isLeader() {
		return leader;
	}

	public void setLeader(boolean leader) {
		this.leader = leader;
		mHandler.sendMessage(Message.obtain(mHandler, MSG_ToggleLeader));
	}

	/**
	 * Called by AT when next generation needs to be created and send send back
	 */
	@Override
	public void sendNextGeneration() {
		System.out.println("++ Sending Next Generation ++");
		if (game.getGenerationPercent() == 100) {
			mHandler.sendMessage(Message.obtain(mHandler, MSG_StopGame, true));
			mHandler.sendMessage(Message.obtain(mHandler, MSG_SendPlayer));
		} else {
			game.nextGeneration();
			mHandler.sendMessage(Message.obtain(mHandler, MSG_UpdateBoard));
			mHandler.sendMessage(Message.obtain(mHandler, MSG_UpdateBoardRemote, game.getJsonCells()));
		}
		
	}

	@Override
	public void startedGame() {
		mHandler.sendMessage(Message.obtain(mHandler, MSG_StartedGame));
	}

	@Override
	public void sendBoard() {
		mHandler.sendMessage(Message.obtain(mHandler, MSG_UpdateBoardRemote, game.getJsonCells()));
	}

	@Override
	public void restartGame() {
		setStarter(false);
		boardView.setTouchable(false);
		running = false;
		leader = false;
		game.restartGame();
		mHandler.sendMessage(Message.obtain(mHandler, MSG_UpdateBoard));
	}
	
	@Override
	public void stopGame() {
		mHandler.sendMessage(Message.obtain(mHandler, MSG_StopGame, false));
		mHandler.sendMessage(Message.obtain(mHandler, MSG_SendPlayer));
		btnShowResults.setVisibility(View.VISIBLE);
	}

	@Override
	public void updatePlayer(String json) {
		game.updatePlayerJson(json);
	}
}
