package me.timwitters.welive;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import me.timwitters.game.Player;
import me.timwitters.game.PlayerCompare;

import com.google.gson.Gson;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class ResultsActivity extends Activity {
	private Gson gson = new Gson();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_results);
		String jsonPlayers = getIntent().getExtras().getString("Players", "");
		System.out.println("Players List: " + jsonPlayers);
		
		TableLayout table = (TableLayout) findViewById(R.id.tableResults);
		
		LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		Player[] playersArray = gson.fromJson(jsonPlayers, Player[].class);
		List<Player> players = Arrays.asList(playersArray);
		
		Collections.sort(players, new PlayerCompare());
		
		for (int i =0; i<players.size(); i++) {
			Player pl = players.get(i);
			TableRow tr = (TableRow) inflater.inflate(R.layout.tabel_row, null);
			((TextView) tr.getChildAt(0)).setText(Integer.toString(i+1));
			((TextView) tr.getChildAt(1)).setText(Integer.toString(pl.getId()));
			((TextView) tr.getChildAt(2)).setText(pl.getName());
			((TextView) tr.getChildAt(3)).setText(Integer.toString(pl.getScore()));
			table.addView(tr);;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.results, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
