package me.timwitters.welive;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity {
	private TextView playerName;
	private SharedPreferences sharedPref;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		playerName = (TextView) findViewById(R.id.txtNameEditField);
		sharedPref = getSharedPreferences("KeyStore",MODE_PRIVATE);
		String username = sharedPref.getString("Username", "");
		playerName.setText(username);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void startGame(View view) {
		System.out.println("Starting Game Activity");
		Intent intent = new Intent(this, GameActivity.class);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putString("Username", playerName.getText().toString());
		editor.commit();
		startActivity(intent);
	}
}
