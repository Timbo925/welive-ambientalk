package me.timwitters.welive;

import java.util.ArrayList;
import java.util.List;

import me.timwitters.game.Cell;
import me.timwitters.game.Game;
import me.timwitters.game.Player;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class BoardView extends View {
	private int squareAmount = 7;
	private int gap = 2;
	private Paint bPaint = new Paint();
	private List<Rect> rectangeles = new ArrayList<Rect>();
	private List<Integer> colors = new ArrayList<Integer>();
	private List<Integer> colorsList = new ArrayList<Integer>();
	private int screenW;
	private int screenH;
	private boolean touchable = false;
	
	
	public BoardView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.init();
	}

	public BoardView(Context context, int squareAmount, int gap) {
		super(context);
		this.squareAmount = squareAmount;
		this.gap = gap;
		this.init();
	}

	/**
	 * Set the measurements of the view to the largest possible square
	 */
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		int widthSize = MeasureSpec.getSize(widthMeasureSpec);
		int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		int heightSize = MeasureSpec.getSize(heightMeasureSpec);
		int size;

		if(widthMode == MeasureSpec.EXACTLY && widthSize > 0){
			size = widthSize;
		}
		else if(heightMode == MeasureSpec.EXACTLY && heightSize > 0){
			size = heightSize;
		}
		else{
			size = widthSize < heightSize ? widthSize : heightSize;
		}

		int finalMeasureSpec = MeasureSpec.makeMeasureSpec(size, MeasureSpec.EXACTLY);
		
		super.onMeasure(finalMeasureSpec, finalMeasureSpec);
	}
	
	public void setSize(int width, int height) {
		squareAmount = width;
		this.init();
	}
	
	
	private void init() {
		colors = new ArrayList<Integer>();
		colorsList.add(Color.parseColor("#616161"));
		colorsList.add(Color.parseColor("#F44336"));
		colorsList.add(Color.parseColor("#2196F3"));
		colorsList.add(Color.parseColor("#4CAF50"));
		colorsList.add(Color.parseColor("#FF9800"));
		colorsList.add(Color.parseColor("#CDDC39"));
		for(int i = 0; i < squareAmount*squareAmount; i++) {
			colors.add(Color.GRAY);
		}
		generateRectangles();
	}
	
	private void generateRectangles() {
		int squareSize = screenW / squareAmount;
		int reqSize = squareSize - (2*gap);
		rectangeles = new ArrayList<Rect>();
		for(int i = 0; i < squareAmount; i++) {
			for(int j = 0; j < squareAmount; j ++) {
				int left = (j * squareSize) + gap;
				int top = (i * squareSize) + gap;
				int right = left + reqSize;
				int bottom = top + reqSize;
				Rect rec = new Rect(left, top, right, bottom);
				rectangeles.add(rec);
			}
		}
	}
	
    @Override
    public void onSizeChanged (int w, int h, int oldw, int oldh){
        super.onSizeChanged(w, h, oldw, oldh);
        screenW = w;
        screenH = h;
        int gapCalc = (int) ((screenW/squareAmount) * 0.08); 
		System.out.println("Gap: " + gapCalc);
		gap = (gapCalc > 3) ? gapCalc : 3;
        this.generateRectangles();
    }

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		bPaint.setStyle(Paint.Style.FILL);
		bPaint.setColor(Color.GRAY);
		for (int i = 0 ; i < rectangeles.size(); i ++) {
			bPaint.setColor(colors.get(i));
			canvas.drawRect(rectangeles.get(i), bPaint);
		}
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (!touchable) {
			return false;
		}
		int touchX = (int) event.getX();
		int touchY = (int) event.getY();
		switch(event.getAction()){
		case MotionEvent.ACTION_DOWN:
			for(Rect rect : rectangeles) {
				if(rect.contains(touchX, touchY)) {
					int index = rectangeles.indexOf(rect);
					colors.set(index, Color.YELLOW);
					this.invalidate();
					((GameActivity) this.getContext()).clickedRectangle(index);
				};
			}
			break;
		}
		return super.onTouchEvent(event);
	}

	public void redraw(Game game) {
		//TODO test if shape changed and redraw
		
		if (rectangeles.size() == game.getCells().size()) {
			for (int i = 0; i < game.getCells().size(); i++) {
				Cell cell = game.getCells().get(i);
				for (Player pl : game.getPlayers()) {
					if (cell.getBelongsTo() == 0) {
						colors.set(i, colorsList.get(0));
					} else if (cell.getBelongsTo() == pl.getId()) {
						colors.set(i, colorsList.get((game.getPlayers().indexOf(pl) + 1)));
					}
				};
			}
		} else {
			System.out.println("Redraw size of board");
		}
		this.invalidate();
	}

	public boolean isTouchable() {
		return touchable;
	}

	public void setTouchable(boolean touchable) {
		this.touchable = touchable;
	}
	
	
	
}
