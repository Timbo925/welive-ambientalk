package me.timwitters.interfaces;

/**
 * Interface with the methods implemented in the LocalInterface of the AT module.
 * Java code will use this to communicate with the AT interpreter.
 * Should be run inside a separate Treat
 * @author Tim
 *
 */
public interface ATInterface {
	public void print(String message);
	public int getPlayerId();
	public void sendTakeCell(int id, int index, long timestamp);
	public void startElection();
	public void goOnline();
	public void goOffline();
	public void startGame();
	public void updateBoardRemote(String jsonBoard, int generationCount);
	public void logATState();
	public void restartGame();
	public void stopGame();
	public void sendPlayer(String json);
}
