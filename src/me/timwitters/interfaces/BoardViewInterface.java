package me.timwitters.interfaces;

/**
 * Interface containing all the methods called from the BoardView 
 * @author Tim
 *
 */
public interface BoardViewInterface {
	void clickedRectangle(int index);
}
