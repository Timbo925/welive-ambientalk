package me.timwitters.interfaces;


/**
 * Interface describing the methods available to be called from the AT module.
 * @author Tim
 *
 */
public interface JInterface {
	void printInJava(String string);
	JInterface registerATApp(ATInterface ati);
	void newATPlayer(int playerId);
	void newRemoteATPlayer(int playerId);
	void updateBoard(String jsonCells, int generationCount);
	void takeCell(int playerId, int index, long timestamp);
	void sendNextGeneration();
	void startedGame();
	void sendBoard();
	void restartGame();
	void stopGame();
	void updatePlayer(String json);
}
