package me.timwitters.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import android.graphics.Color;
import android.os.PowerManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


public class Game {
	private int gameWidth;
	private int gameHeight;
	private List<Player> players = new ArrayList<Player>();
	private List<Cell> cells = new ArrayList<Cell>();
	private int liveBank;
	private int generationCount = 0;
	private int generationMax = 10;
	
	public Game(int gameWidth, int gameHeight) {
		super();
		this.gameWidth = gameWidth;
		this.gameHeight = gameHeight;
		this.liveBank = 5;
		this.generateCells();
	}

	public String getJsonCells() {
		Gson gson = new Gson();
		return gson.toJson(cells);
	}
	
	public String getJsonGame() {
		Gson gson = new Gson();
		return gson .toJson(this);
	}
	
	public void updateBoardJson(String json) {
		Gson gson = new Gson();
		Cell[] cellArray = gson.fromJson(json, Cell[].class);
		cells = Arrays.asList(cellArray);
	}
	
	public boolean newPlayer(Player player) {
		if (containsPlayer(player) < 0) {
			players.add(player);
			System.out.println("Adding player, Player List: " + players.toString());
		} else {
			System.out.println("Player still in list");
			return false;
		}
		return true;
	}
	
	public List<Cell> aroundCellIndex(int i) {
		List<Cell> aroundList = new ArrayList<Cell>();
		for(int j = -1; j < 2; j++) {
			for(int z = -1; z < 2; z++) {
				int toAdd = i+(j*this.gameWidth)+z;
				//
				if (toAdd >= 0 && toAdd <= (gameHeight*gameWidth) - 1 && toAdd != i) {
					if (!((i % gameWidth == 0) && z == -1)) {
						if(!((i % gameWidth == gameWidth - 1 ) && z == 1)) {
							//System.out.println("Adding: " + toAdd);
							aroundList.add(cells.get(toAdd));
						}
					}
				}
			}	
		}
		return aroundList;
	}
	
	public void takeCell(int playerId, int cellIndex, long timestamp) {
		Cell cell = cells.get(cellIndex);
		switch(cell.getCellStatus()) {
		case DEAD:
			Player pl = getPlayerWithId(playerId);
			if (pl != null) {
				System.out.println("Taking cell: "+ cellIndex);
				cell.setBelongsTo(playerId);
				cell.setCellStatus(CellStatus.ALIVE);
				cell.setTimestamp(timestamp);
				if(pl == players.get(0)) {
					liveBank = liveBank -1;
				}
			} else {
				System.out.println("Player with id: " + playerId + " not found");
			}
			break;
		case ALIVE:
			System.out.println("Taken");
			if (getPlayerWithId(playerId) != null && cell.getTimestamp() > timestamp) {
				System.out.println("Overtaken");
				liveBank += 1; // Cell overtaken due to conflict resolution, gain back live.
				cell.setBelongsTo(playerId);
				cell.setTimestamp(timestamp);
			}
			break;
		}
	}
	
	public int containsPlayer(Player player) {
		for(Player pl : players) {
			if (pl.getId() == player.getId()) {
				return players.indexOf(pl);
			}
		}
		return -1;
	}
	
	public Player getPlayerWithId(int id) {
		for(Player pl: players) {
			if (pl.getId() == id) {
				return pl;
			}
		}
		return null;
	}
	
	
	public void nextGeneration() {
		setGenerationCount(generationCount + 1);
		List<Cell> newCells = new ArrayList<Cell>();
		for(int i = 0; i < cells.size(); i++) {
			Cell cell = new Cell(cells.get(i));
			List<Cell> aroundList = aroundCellIndex(i);
			int aliveCount = countLives(aroundList);
			//System.out.println("Around index: " + i + " size: " + aroundList.size() + " live count:" + aliveCount);
			if (cell.getCellStatus() == CellStatus.ALIVE) {
				if(aliveCount < 2) {
					cell.setBelongsTo(0);
					cell.setCellStatus(CellStatus.DEAD);
					newCells.add(cell);
				} else if (aliveCount <= 3) {
					newCells.add(cell);
				} else if (aliveCount > 3) {
					cell.setBelongsTo(0);
					cell.setCellStatus(CellStatus.DEAD);
					newCells.add(cell);
				}
			} else if (cell.getCellStatus() == CellStatus.DEAD) {
				if(aliveCount == 3) {
					cell.setCellStatus(CellStatus.ALIVE);
					cell.setTimestamp(System.currentTimeMillis());
					cell.setBelongsTo(winnerCells(aroundList));
					newCells.add(cell);
				} else {
					newCells.add(cell);
				}
			}
		}
		cells = newCells;
	}
	
	public void printConsole() {
		for(int i = 0; i< this.gameHeight; i++) {
			String line = "";
			for(int j=0; i< this.gameWidth; j++) {
				line += (cells.get(i*j).getCellStatus() == CellStatus.ALIVE) ? "X" : "O";
			}
			System.out.println(line);
		}
	}
	
	public int winnerCells(List<Cell> aroundCells) {
		HashMap<Integer, Integer> voteList = new HashMap<Integer, Integer>();
		for(Player player: this.players) {
			voteList.put(player.getId(), 0);
		}
		//Adding a vote to each id.
		for(Cell cell : aroundCells) {
			if (cell.getBelongsTo() != 0) {
				voteList.put(cell.getBelongsTo(), voteList.get(cell.getBelongsTo()) + 1);
			}
		}
		
		int currentMostVotes = 0;
		int currentBestId = 0;
		System.out.println("VoteList" + voteList.toString());
		for(Entry<Integer, Integer> vote : voteList.entrySet()) {
			if(currentMostVotes < vote.getValue()) {
				currentMostVotes = vote.getValue();
				currentBestId = vote.getKey();
			}
		}
		return currentBestId;
	}
	
	public int countLives(List<Cell> cells) {
		int count = 0;
		for (Cell cell: cells) {
			if (cell.getCellStatus() == CellStatus.ALIVE) {
				count += 1;
			}
		}
		return count;
	}
	
	public boolean removePlayerWithId(int id) {
		for(Player pl : players) {
			if(pl.getId() == id) {
				players.remove(pl);
				return true;
			}
		}
		return false;
	}
	
	public void generateCells() {
			cells = new ArrayList<Cell>();
		for(int i = 0; i < gameHeight*gameWidth; i ++) {
			//TODO change back to INACTIVE
			cells.add(new Cell(CellStatus.DEAD));
		}
	}

	public int getGameWidth() {
		return gameWidth;
	}

	public void setGameWidth(int gameWidth) {
		this.gameWidth = gameWidth;
	}

	public int getGameHeight() {
		return gameHeight;
	}

	public void setGameHeight(int gameHeight) {
		this.gameHeight = gameHeight;
	}

	
	public List<Player> getPlayers() {
		players.get(0).setScore(getScore());
		return players;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}

	public List<Cell> getCells() {
		return cells;
	}

	public void setCells(List<Cell> cells) {
		this.cells = cells;
	}

	public int getLiveBank() {
		return liveBank;
	}

	public void setLiveBank(int liveBank) {
		this.liveBank = liveBank;
	}
	
	public void getMyPlayer() {
		Player player = players.get(0);
		player.setScore(getScore());	
	}
	
	public String getMyPlayerJson() {
		Gson gson = new Gson();
		return gson.toJson(players.get(0));
	}
	
	public void updatePlayer(Player player) {
		//TODO update player in list.
	}
	
	public int getScore() {
		int score = 0;
		for(Cell cell: cells) {
			if(players.get(0).getId() == cell.getBelongsTo()) {
				score += 10;
			}
		}
		return score;
	}

	public int getGenerationCount() {
		return generationCount;
	}

	public void setGenerationCount(int generationCount) {
		if(generationCount % 2 == 0 && generationCount != 0) {
			liveBank += 5;
		}
		this.generationCount = generationCount;
	}

	public void restartGame() {
		this.generationCount = 0;
		this.liveBank = 5;
		this.generateCells();
	}
	
	public int getGenerationMax() {
		return this.generationMax;
	}

	public int getGenerationPercent() {
		return (generationCount*100/generationMax);
	}

	public void updatePlayerJson(String json) {
		Gson gson = new Gson();
		Player player = gson.fromJson(json, Player.class);
		Player toUpdatePlayer = getPlayerWithId(player.getId());
		toUpdatePlayer.setName(player.getName());
		toUpdatePlayer.setScore(player.getScore());
	}
	
	
}
