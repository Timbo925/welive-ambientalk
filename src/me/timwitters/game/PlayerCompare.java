package me.timwitters.game;

import java.util.Comparator;
import me.timwitters.game.*;

public class PlayerCompare implements Comparator<Player> {

	@Override
	public int compare(Player p1, Player p2) {
		 return (p1.getScore() < p2.getScore())? 1: -1;
	}

}
