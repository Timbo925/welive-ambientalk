package me.timwitters.game;

public class Cell {
	private CellStatus cellStatus = CellStatus.DEAD;
	private int belongsTo = 0;
	private long timestamp;
	
	public Cell() {	
	};
	
	public Cell(Cell cell) {
		super();
		this.cellStatus = cell.cellStatus;
		this.belongsTo = cell.belongsTo;
		this.timestamp = cell.timestamp;
	}

	public Cell(CellStatus cellStatus) {
		super();
		this.cellStatus = cellStatus;
	}
	
	public CellStatus getCellStatus() {
		return cellStatus;
	}
	public void setCellStatus(CellStatus cellStatus) {
		this.cellStatus = cellStatus;
	}
	public int getBelongsTo() {
		return belongsTo;
	}
	public void setBelongsTo(int playerId) {
		this.belongsTo = playerId;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
}
